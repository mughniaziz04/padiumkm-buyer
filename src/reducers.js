import {combineReducers} from 'redux';
import homeReducer from './screens/home/reducer';

const rootReducer = combineReducers({
  homeReducer,
});

export default rootReducer;
