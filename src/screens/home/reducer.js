import * as CONST from './constant';

const initialState = {
  count: 0,
};

const homeReducer = (state = initialState, action) => {
  const {payload, type} = action;
  switch (type) {
    case CONST.CHANGE_COUNT:
      return {
        ...state,
        count: payload,
      };
    default:
      return state;
  }
};

export default homeReducer;
