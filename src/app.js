import React from 'react';
import Route from './routes';

function App() {
  return <Route key="route" />;
}

export default App;
