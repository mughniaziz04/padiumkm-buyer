import * as CONST from './constant';

export function changeCount(count) {
  return {
    type: CONST.CHANGE_COUNT,
    payload: count,
  };
}
