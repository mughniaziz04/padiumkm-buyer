import React from 'react';
import {View, Button, Text} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {changeCount} from './action';
import {IconAttachment} from './../../assets';
import {styles} from './styles';

function Home() {
  const dispatch = useDispatch();
  const count = useSelector(state => state.homeReducer.count);
  return (
    <View style={styles.container}>
      <Text>Its time to Count</Text>
      <Button
        title="Increase"
        onPress={() => dispatch(changeCount(count + 1))}
      />
      <Text>{count}</Text>
      <Button
        title="Decrease"
        onPress={() => dispatch(changeCount(count - 1))}
      />
      <IconAttachment />
    </View>
  );
}

export default Home;
